﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Calculator.Operations;

namespace Calculator.Tests
{
    [TestFixture]
    class SolverTest
    {
        private static OperationRegistry binaryOperationRegistry;
        private static OperationRegistry unaryOperationRegistry;
        private Solver solver;

        [TestFixtureSetUp]
        public void SetUp()
        {
            binaryOperationRegistry = new OperationRegistry();
            binaryOperationRegistry.RegisterOperation(new AdditionOperation());
            binaryOperationRegistry.RegisterOperation(new SubtractionOperation());
            binaryOperationRegistry.RegisterOperation(new OpenBracketOperation());
            binaryOperationRegistry.RegisterOperation(new CloseBracketOperation());
            binaryOperationRegistry.RegisterOperation(new MultiplicationOperation());
            binaryOperationRegistry.RegisterOperation(new DivisionOperation());

            unaryOperationRegistry = new OperationRegistry();
            unaryOperationRegistry.RegisterOperation(new NegationOperation());

            solver = new Solver(unaryOperationRegistry, binaryOperationRegistry);
        }

        [Test]
        public void EmptyInput()
        {            
            string result = solver.Solve(String.Empty);
            Assert.AreEqual("0", result);

            result = solver.Solve("");
            Assert.AreEqual("0", result);

            result = solver.Solve(null);
            Assert.AreEqual("0", result);
        }

        [Test]
        public void SpacesEverywhere()
        {
            string result = solver.Solve("   ");
            Assert.AreEqual("0", result);

            result = solver.Solve("   -  5   + (    5 - 7)*   8 / 4   ");
            Assert.AreEqual("-9", result);
        }
        [Test]
        public void ExampleFromEMail()
        {
            string result = solver.Solve("1+2-3");
            Assert.AreEqual("0", result);
        }

        [Test]
        public void ModifiedExampleFromEMail()
        {
            string result = solver.Solve("1-(2+3)");
            Assert.AreEqual("-4", result);
        }

        [Test]
        public void NegationAndAddition()
        {
            string result = solver.Solve("-2+3");
            Assert.AreEqual("1", result);
        }

        [Test]
        public void NegationAfterOpeningBracket()
        {
            string result = solver.Solve("-6+(-7)");
            Assert.AreEqual("-13", result);
        }

        [Test]
        public void RealArguments()
        {
            string result = solver.Solve("2.05+10.07");
            Assert.AreEqual("12.12", result);
        }

        [Test]
        public void MultiplicationAndAdditive()
        {
            string result = solver.Solve("-2*(3-6)+2+2*2");
            Assert.AreEqual("12", result);
        }

        [Test]
        public void DivisionByZero()
        {
            string result = solver.Solve("7/0");
            Assert.AreEqual("Infinity", result);

            result = solver.Solve("-3/0");
            Assert.AreEqual("-Infinity", result);

            result = solver.Solve("1/0-1/0");
            Assert.AreEqual("NaN", result);
        }

        [Test]
        public void BracketFirst()
        {
            string result = solver.Solve("(2+6)/2");
            Assert.AreEqual("4", result);
        }

        [Test]
        public void BracketsInBrackets()
        {
            string result = solver.Solve("((2+6)*(2+2)) * 2");
            Assert.AreEqual("64", result);

            result = solver.Solve("((2+6)*(2+2) - 7) * 2");
            Assert.AreEqual("50", result);

            result = solver.Solve("(-(2+6)*(2+2) - 7) * 2");
            Assert.AreEqual("-78", result);
        }

        [Test]
        [ExpectedException(typeof(WrongExpressionException))]
        public void IncorrectOperation()
        {
            string result = solver.Solve("((2+6)*2) | 2");
        }

        [Test]
        [ExpectedException(typeof(WrongExpressionException))]
        public void MoreClosingBrackets()
        {
            //не хватает открывающей скобки
            string result = solver.Solve("(2+6)*(2+2)) - 2");
        }

        [Test]
        [ExpectedException(typeof(WrongExpressionException))]
        public void SingleClosingBracket()
        {
            string result = solver.Solve(")");
        }

        [Test]
        [ExpectedException(typeof(WrongExpressionException))]
        public void MoreOpeningBrackets()
        {
            string result = solver.Solve("((2+6)*(2+2) + 2");
        }
    }
}
