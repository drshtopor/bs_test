﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Calculator.Operations;

namespace Calculator.Tests
{
    [TestFixture]
    class OperationTest
    {
        [Test]
        public void AdditionOperationTest()
        {
            double a = 3; double b = 2;
            AdditionOperation addOp = new AdditionOperation();
            Assert.AreEqual(a + b, addOp.performOperation(a, b));
            Assert.AreEqual(b + a, addOp.performOperation(b, a));
        }

        [Test]
        public void AdditionSymbolTest()
        {
            IOperation addOp = new AdditionOperation();
            Assert.AreEqual("+", addOp.getOperationSymbol());
        }

        [Test]
        public void SubtractionOperationTest()
        {
            double a = 3; double b = 2;
            SubtractionOperation subOp = new SubtractionOperation();
            Assert.AreEqual(a - b, subOp.performOperation(a, b));
            Assert.AreEqual(b - a, subOp.performOperation(b, a));
        }

        [Test]
        public void SubtractionSymbolTest()
        {
            IOperation subOp = new SubtractionOperation();
            Assert.AreEqual("-", subOp.getOperationSymbol());
        }

        [Test]
        public void MultiplicationOperationTest()
        {
            double a = 3; double b = 2;
            MultiplicationOperation mulOp = new MultiplicationOperation();
            Assert.AreEqual(a * b, mulOp.performOperation(a, b));
            Assert.AreEqual(b * a, mulOp.performOperation(b, a));
        }

        [Test]
        public void MultiplicationSymbolTest()
        {
            IOperation mulOp = new MultiplicationOperation();
            Assert.AreEqual("*", mulOp.getOperationSymbol());
        }

        [Test]
        public void DivisionOperationTest()
        {
            double a = 3; double b = 2;
            DivisionOperation divOp = new DivisionOperation();
            Assert.AreEqual(a / b, divOp.performOperation(a, b));
            Assert.AreEqual(b / a, divOp.performOperation(b, a));
        }

        [Test]
        public void DivisionSymbolTest()
        {
            IOperation divOp = new DivisionOperation();
            Assert.AreEqual("/", divOp.getOperationSymbol());
        }

        [Test]
        public void OperationPriorityTest()
        {
            IOperation addOp = new AdditionOperation();
            Assert.AreEqual(OperationPriority.Additive, addOp.getPriority());

            IOperation subOp = new SubtractionOperation();
            Assert.AreEqual(OperationPriority.Additive, subOp.getPriority());

            IOperation negOp = new NegationOperation();
            Assert.AreEqual(OperationPriority.Unary, negOp.getPriority());

            IOperation openBrk = new OpenBracketOperation();
            Assert.AreEqual(OperationPriority.OpenBracket, openBrk.getPriority());

            IOperation closeBrk = new CloseBracketOperation();
            Assert.AreEqual(OperationPriority.CloseBracket, closeBrk.getPriority());

            IOperation mulOp = new MultiplicationOperation();
            Assert.AreEqual(OperationPriority.Multiplicative, mulOp.getPriority());

            IOperation divOp = new DivisionOperation();
            Assert.AreEqual(OperationPriority.Multiplicative, divOp.getPriority());
        }

        [Test]
        public void PriorityOrderTest()
        {
            Assert.Less(OperationPriority.OpenBracket, OperationPriority.CloseBracket);
            Assert.Less(OperationPriority.CloseBracket, OperationPriority.Additive);
            Assert.Less(OperationPriority.Additive, OperationPriority.Multiplicative);
            Assert.Less(OperationPriority.Multiplicative, OperationPriority.Unary);
        }
    }
    
}
