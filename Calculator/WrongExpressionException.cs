﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Calculator
{
    class WrongExpressionException : Exception
    {
        public WrongExpressionException(string message) : base(message) { }
    }
}
