﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Calculator.Operations;
using System.Text.RegularExpressions;
using System.Globalization;

namespace Calculator
{
    class Solver
    {
        private OperationRegistry binaryOperationRegistry;
        private OperationRegistry unaryOperationRegistry;

        private Stack<double> calcStack;
        private Stack<IOperation> opStack;

        private Regex numberRegex;

        private bool nextOperationMayBeUnary;

        private int findAndPushNumber(string tail)
        {
            if (numberRegex.IsMatch(tail))
            {
                StringBuilder foundNumber = new StringBuilder(numberRegex.Match(tail).Value);
                calcStack.Push(double.Parse(foundNumber.ToString(), NumberFormatInfo.InvariantInfo));
                return foundNumber.Length;
            }
            return 0;
        }

        private IOperation findOperation(string tail, OperationRegistry registry)
        {
            foreach (var rec in registry.Operations)
            {
                if (tail.IndexOf(rec.Value.getOperationSymbol()) == 0)
                {
                    return rec.Value;
                }
            }
            return null;
        }

        public Solver(OperationRegistry unaryOperationRegistry, OperationRegistry binaryOperationRegistry)
        {
            this.unaryOperationRegistry = unaryOperationRegistry;
            this.binaryOperationRegistry = binaryOperationRegistry;

            calcStack = new Stack<double>();
            opStack = new Stack<IOperation>();

            numberRegex = new Regex(@"^\d+((\,|\.)\d*)?");
        }

        public string Solve(string input)
        {
            if (input == null)
                return "0";
            
            input = input.Replace(" ", string.Empty);
            
            if (input == string.Empty)
                return "0";
            
            nextOperationMayBeUnary = true;

            int pos = 0;
            while (pos < input.Length)
            {
                string tail = input.Substring(pos);

                int symbols = findAndPushNumber(tail);
                if (symbols != 0)
                {
                    pos += symbols;
                    nextOperationMayBeUnary = false;
                    continue;
                }

                IOperation op = null;
                if (nextOperationMayBeUnary)
                    op = findOperation(tail, unaryOperationRegistry);
                if (op == null)
                    op = findOperation(tail, binaryOperationRegistry);

                if (op == null)
                {
                    StringBuilder errormsg = new StringBuilder("Недопустимая операция: ");
                    errormsg.Append(input[pos]);
                    throw new WrongExpressionException(errormsg.ToString());
                }

                nextOperationMayBeUnary = false;
                pos += op.getOperationSymbol().Length;

                if (op is OpenBracketOperation)
                {
                    opStack.Push(op);
                    nextOperationMayBeUnary = true;
                    continue;
                }
                if (op is CloseBracketOperation)
                {
                    IOperation topOfStack = null;
                    try
                    {
                       topOfStack  = opStack.Pop();
                    }
                    catch (InvalidOperationException)
                    {
                        throw new WrongExpressionException("В выражении не хватает открывающей скобки");
                    }

                    while (!(topOfStack is OpenBracketOperation))
                    {
                        topOfStack.performOperation(calcStack);
                        if (opStack.Count > 0)
                            topOfStack = opStack.Pop();
                        else
                            throw new WrongExpressionException("В выражении не хватает открывающей скобки");
                    }
                    continue;
                }
                if (op is UnaryOperation)
                {
                    opStack.Push(op);
                    continue;
                }
                if (op is BinaryOperation)
                {
                    while ((opStack.Count > 0) && (op as BinaryOperation).hasLowerPriorityThan(opStack.Peek()))
                    {
                        opStack.Pop().performOperation(calcStack);
                    }
                    opStack.Push(op);
                    continue;
                }
            }

            while (opStack.Count > 0)
            {
                try
                {
                    opStack.Pop().performOperation(calcStack);
                }
                catch (NotImplementedException e)
                {
                    if (e.Message.Contains("opening"))
                        throw new WrongExpressionException("В выражении не хватает закрывающей скобки");
                    else
                        throw new WrongExpressionException("Ошибка в выражении");
                }
            }

            return calcStack.Pop().ToString(NumberFormatInfo.InvariantInfo);
        }

    }
}
