﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator.Operations
{
    class OpenBracketOperation : IOperation
    {
        public string getOperationSymbol()
        {
            return "(";
        }

        public OperationPriority getPriority()
        {
            return OperationPriority.OpenBracket;
        }

        public void performOperation(Stack<double> stack)
        {
            throw new NotImplementedException("Can't execute opening bracked");
        }
    }
}
