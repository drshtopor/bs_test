﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator.Operations
{
    interface IOperation
    {
        string getOperationSymbol();

        OperationPriority getPriority();

        void performOperation(Stack<double> stack);
    }
}
