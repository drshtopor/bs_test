﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator.Operations
{
    class CloseBracketOperation: IOperation
    {
        public string getOperationSymbol()
        {
            return ")";
        }

        public OperationPriority getPriority()
        {
            return OperationPriority.CloseBracket;
        }

        public void performOperation(Stack<double> stack)
        {
            throw new NotImplementedException("Can't execute closing bracket");
        }
    }
}
