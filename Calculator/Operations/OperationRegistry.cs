﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator.Operations
{
    class OperationRegistry
    {
        public Dictionary<string, IOperation> Operations { get; private set; }

        public OperationRegistry()
        {
            Operations = new Dictionary<string, IOperation>();
        }

        public void RegisterOperation(IOperation op)
        {
            Operations.Add(op.getOperationSymbol(), op);
        }
    }
}
