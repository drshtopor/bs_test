﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator.Operations
{
    abstract class BinaryOperation: IOperation
    {
        /// <summary>
        /// Выполнить операцию.
        /// Левый и правый операнды различаются для некоммуникативных операций (возведение в степень, ...)
        /// </summary>
        /// <param name="left">левый операнд</param>
        /// <param name="right">правый операнд</param>
        /// <returns>результат выполнения операции</returns>
        abstract public double performOperation(double left, double right);

        abstract public bool isRightAssociated();

        public bool hasLowerPriorityThan(IOperation other)
        {
            if (this.isRightAssociated())
                return this.getPriority() < other.getPriority();
            else
                return this.getPriority() <= other.getPriority();
        }

        abstract public string getOperationSymbol();

        abstract public OperationPriority getPriority();


        public void performOperation(Stack<double> stack)
        {
            double right = stack.Pop();
            double left = stack.Pop();
            stack.Push(performOperation(left, right));
        }
    }
}
