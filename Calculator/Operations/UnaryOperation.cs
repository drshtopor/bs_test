﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator.Operations
{
    abstract class UnaryOperation :IOperation
    {
        abstract public double performOperation(double operand);

        public void performOperation(Stack<double> stack)
        {
            double operand = stack.Pop();
            stack.Push(performOperation(operand));
        }

        abstract public string getOperationSymbol();

        abstract public OperationPriority getPriority();
    }
}
