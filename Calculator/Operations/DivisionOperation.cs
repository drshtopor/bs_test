﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Calculator.Operations
{
    class DivisionOperation: BinaryOperation
    {
        public override double performOperation(double left, double right)
        {
            return left / right;
        }

        public override bool isRightAssociated()
        {
            return false;
        }

        public override string getOperationSymbol()
        {
            return "/";
        }

        public override OperationPriority getPriority()
        {
            return OperationPriority.Multiplicative;
        }
    }
}
