﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator.Operations
{
    class NegationOperation: UnaryOperation
    {
        public override double performOperation(double operand)
        {
            return - operand;
        }

        public override string getOperationSymbol()
        {
            return "-";
        }

        public override OperationPriority getPriority()
        {
            return OperationPriority.Unary;
        }
    }
}
