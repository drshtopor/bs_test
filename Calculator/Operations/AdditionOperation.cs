﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator.Operations
{
    class AdditionOperation: BinaryOperation
    {

        public override double performOperation(double left, double right)
        {
            return left + right;
        }

        public override string getOperationSymbol()
        {
            return "+";
        }

        public override OperationPriority getPriority()
        {
            return OperationPriority.Additive;
        }


        public override bool isRightAssociated()
        {
            return false;
        }
    }
}
