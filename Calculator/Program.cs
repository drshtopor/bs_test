﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Calculator.Operations;


namespace Calculator
{
    class Program
    {
        private static OperationRegistry binaryOperationRegistry;
        private static OperationRegistry unaryOperationRegistry;
        private static Solver solver;

        static void Init()
        {
            binaryOperationRegistry = new OperationRegistry();
            binaryOperationRegistry.RegisterOperation(new AdditionOperation());
            binaryOperationRegistry.RegisterOperation(new SubtractionOperation());
            binaryOperationRegistry.RegisterOperation(new OpenBracketOperation());
            binaryOperationRegistry.RegisterOperation(new CloseBracketOperation());
            binaryOperationRegistry.RegisterOperation(new MultiplicationOperation());
            binaryOperationRegistry.RegisterOperation(new DivisionOperation());

            unaryOperationRegistry = new OperationRegistry();
            unaryOperationRegistry.RegisterOperation(new NegationOperation());

            solver = new Solver(unaryOperationRegistry, binaryOperationRegistry);
        }

        static void Main(string[] args)
        {
            Init();

            Console.Write(">");
            string input = Console.ReadLine();
            try
            {
                Console.WriteLine(">{0}", solver.Solve(input));
            }
            catch (WrongExpressionException ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}
